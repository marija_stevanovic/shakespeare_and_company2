<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class Book extends Model
{
    protected $fillable = [
        'naslov',
        'autor',
        'slika',
        'category_id',
    ];

    public function category(){
        return $this->belongsTo(Category::class);
    }
}
