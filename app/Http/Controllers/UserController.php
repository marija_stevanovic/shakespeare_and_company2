<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{
    public function index()
    {
        $user = User::all();

        return response()->json(['data'=>$user], 200);
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        return response()->json(['data'=>$user], 200);
    }

    public function kreirajBibliotekare($id)
    {
        $user = User::findOrFail($id);
        $user->assignRole('bibliotekar');

        $bibliotekar = User::role('bibliotekar')->get();

        return response()->json(['data'=>$bibliotekar], 200);
    }

    public function kreirajPomocnike($id)
    {
        $user = User::findOrFail($id);
        $user->assignRole('pomocnik bibliotekara');

        $pomocnik = User::role('pomocnik bibliotekara')->get();

        return response()->json(['data'=>$pomocnik], 200);
    }
}
