<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    public function show($id)
    {
        dd(Auth::user());
        $category = Category::findOrFail($id);

        $categories = Category::whereNull('parent_id')
            ->with('childrenCategories')
            ->get();


        $category->books;

        return response()->json(['data'=>$category], 200);


    }

    public function listCatBooks()
    {
        $categories = Category::paginate(15);

        foreach ($categories as $category){
            $category->books;
        }

        return response()->json(['data'=>$categories], 200);
    }
}
