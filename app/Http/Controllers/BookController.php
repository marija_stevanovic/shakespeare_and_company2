<?php

namespace App\Http\Controllers;
use App\Book;

use App\Category;
use App\Http\Requests\Books\CreateBooksRequest;
use Illuminate\Http\Request;

class BookController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    public function index()
    {
        $books = Book::all();

        foreach ($books as $book){
            $book->category->naziv;
        }

        return response()->json(['data'=>$books], 200);
    }

    public function show($id)
    {
        $book = Book::findOrFail($id);
        return response()->json(['data'=>$book], 200);

    }


    public function store(CreateBooksRequest $request)
    {
        $book = Book::create($request->all());

        return response()->json(['data'=>$book], 200);
    }

    public function update($id, Request $request)
    {
        $book = Book::findOrFail($id);

        $book->update($request->all());

        return response()->json(['data'=>$book], 200);
    }


    public function destroy($id)
    {
        $book = Book::findOrFail($id);
        $book->delete();

        return response()->json(['data'=>$book], 200);
    }
}
