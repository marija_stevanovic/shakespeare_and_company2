<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Book;
use App\Category;
use Faker\Generator as Faker;

$factory->define(Book::class, function (Faker $faker) {
    return [
        'naslov'=>$faker->word,
        'autor'=>$faker->name,
        'slika'=>$faker->image,
        'category_id'=>Category::all()->id,
    ];
});
