<?php

use Illuminate\Database\Seeder;
use App\Book;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        for ($i=0; $i<20; $i++){
            Book::create([
                'naslov' => $faker->name,
                'autor' => $faker->name,
                'slika' => $faker->image,
                'category_id' => rand(0,12)
            ]);
        }
    }
}
