<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use App\User;


class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role1 = Role::create(['name'=>'bibliotekar']);
        $role2 = Role::create(['name'=>'pomocnik bibliotekara']);
        $role3 = Role::create(['name'=>'super-admin']);

        $permission1 = Permission::create(['name'=>'edit books']);
        $permission2 = Permission::create(['name'=>'publish books']);
        $permission3 = Permission::create(['name'=>'delete books']);

        $role1->givePermissionTo($permission2);
        $role1->givePermissionTo($permission3);
        $role1->givePermissionTo($permission1);

        $role2->givePermissionTo($permission1);

        

    }
}
