<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        for ($i=0; $i<20; $i++){
            Category::create([
                'naziv' => $faker->name,
                'parent_id' => rand(0,12)
            ]);
        }

    }
}
