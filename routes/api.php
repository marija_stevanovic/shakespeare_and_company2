<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/
Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@authenticate');
    Route::get('open', 'DataController@open');


Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('user', 'AuthController@getAuthenticatedUser');
    Route::get('closed', 'DataController@closed');
});

Route::get('users', 'UserController@index');
Route::get('bibliotekari/{id}', 'UserController@kreirajBibliotekare')->middleware('role:super-admin');
Route::get('pomocnici/{id}', 'UserController@kreirajPomocnike')->middleware('role:super-admin');
Route::get('users/{id}', 'UserController@show');
Route::get('books', 'BookController@index');
Route::get('books/{id}', 'BookController@show');
Route::get('categories', 'CategoryController@listCatBooks')->middleware('auth');
Route::get('categories/{id}', 'CategoryController@show');
Route::get('books/{id}', 'BookController@show');



Route::group(['middleware' => ['role:bibliotekar','permission:publish book|edit book|delete book']], function(){
    Route::delete('books-delete/{id}', 'BookController@destroy');
    Route::post('books-store', 'BookController@store');
    Route::put('book-update/{id}', 'BookController@update');
});

Route::group(['middleware' => ['role:pomocnik bibliotekara|edit books']], function () {
    Route::put('book-update/{id}', 'BookController@update');
});

